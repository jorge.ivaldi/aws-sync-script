# ----------------------------------
# --- simbolos para denotar acciones
$proccessItem= "[#]"
$proccessResponse="[=]"
$proccessDebug="[d]"
$proccessDebugCredentials="[v]"
$proccessAwsSync = "[Sync AWS S3]"
$proccessDebugParams ="[?]"

# ----------------------------------------
# --- definición de argumentos validos ---
$params = @{
    pwd = @('--password','--pwd','-pwd','-password');
    verbose = @('--verbose','--v','-v','-verbose');
    clipboard = @('--clipboard','-clipboard');
    debug_params = @('--debug-params','-debug-params');
    help = @('--help','-h','/?','-?');
}

# --------------------------------------------------------------------
# --- hashtable que guarda indice, valor y si fue seteado el argumento
# --- según los argumentos que haya recibido el script
$args_params = @{
    pwd = @{ index = -1; is_required = $true; require_value = $true; setted = $false; value = "" };
    clipboard = @{ index = -1; is_required = $false; require_value = $false; setted = $false; value = "" };
    help = @{ index = -1; require_value = $false; setted = $false; value = "" };
}

# -------------------------------------------------------
# --- definición de argumentos que no requieren valor ---
$params_without_values = $args_params.GetEnumerator() | ForEach-Object {
    if(-not $_.value.require_value) { $_.key }
}


$debug_params = $false
if(($args.Count -gt 0)){
    foreach ($param in $params.debug_params) {
        $index = [array]::IndexOf($args, $param)
        if($index -gt -1) {
            $debug_params = $true
            break
        }
    }
}
# ----------------------------------------------------
# --- lugar donde alojo los valores de los argumentos
# --- recibidos en el script
$values = @{
    pwd = "";
    clipboard = $false;
    verbose = $false;
}

Write-Host ""
Write-Host "------------------------"
Write-Host "#    Cypher Password   #"
Write-Host "------------------------"
Write-Host "~"


# si recibió argumentos
if ($args.Count -gt 0){
    # navego cada param key
    foreach($param_key in $params.keys) {
        # Write-Host "[-] posible params: " $params[$param_key] # debug
        # por cada valor en los valores
        # Write-Host ">> param: $param_key" # debug
        foreach($param in $params[$param_key]) {
            $index = [array]::IndexOf($args, $param)
            if(($index -gt -1)) {
                # Write-Host "$proccessDebugParams $param in arguments" # debug
                # si no existe elemento es porque es un parametro especial
                if(-not $args_params[$param_key]) { continue }
                if(-not $args_params[$param_key].setted) {
                    $args_params[$param_key].index = $index
                    if($args_params[$param_key].require_value) {
                        $value_index = $args_params[$param_key].index + 1

                        $param_value = $args[$value_index]
                        $is_empty = [string]::IsNullOrEmpty($param_value)
                        if($debug_params) { Write-Host "$proccessDebugParams Param found: $param, value: $param_value" }

                        # Write-Host "$value_index, $is_empty" # debug
                        if($is_empty) {
                            throw "$param not has value"
                        } else {
                            if($param_value -like '-*'){
                                throw "$param not has value"
                            }
                        }
                        $args_params[$param_key].value = $param_value
                    } else {
                        if($param_key -in $params_without_values) {
                            if($param_key -like "*debug*") {
                                $readable_param = $param_key.replace('_',' ')
                                Write-Host "$proccessDebugParams $readable_param enabled"
                            } # debug
                            $args_params[$param_key].setted = $true
                            continue
                        }

                        if($debug_params) {
                            Write-Host "$proccessDebugParams $param"
                        } # debug
                    }
                    $args_params[$param_key].setted = $true
                }
            }
        }
    }
}

$verify_args = $true
if($args_params.help.setted) {
    $verify_args = $false
    Write-Host "$proccessDebugParams Argument debug"
    Write-Host "$proccessItem Current Proccess"
    Write-Host "$proccessDebug Proccess Debug"
    Write-Host "$proccessDebugCredentials Proccess Debug Credentials"
    Write-Host "$proccessResponse Proccess Response"
    Write-Host "~"
    Write-Host ""
}

foreach ($param in $args_params.keys) {
    if(($param -ne "help") -and $verify_args) {
        if($args_params[$param].setted) {
            if($args_params[$param].require_value) {
                $values[$param] = $args_params[$param].value
            } else {
                $values[$param] = $args_params[$param].setted
            }
        } else {
            if($args_params[$param].is_required) {
                $_param = $params[$param]
                throw "$param not found and is required, use someone of this params to set it: $_param"
            }
        }
    }
}

if($args_params.help.setted) {
    Write-Host "Params accepted:"
    foreach ($param in $params.keys) {
        $readable_param = $param.replace('_',' ')
        if($param -eq 'src') {
            $readable_param = 'source folder'
        }
        Write-Host "- For set $readable_param option: " ($params[$param] -join ", ")
    }
    Write-Host ""
    exit 0
}

$pwd = $values.pwd
$cypherPass = [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($pwd))
if($values.verbose) {
    Write-Host "$proccessItem password: $pwd"
    Write-Host "$proccessResponse cypher: $cypherPass"
    Write-Host ""
}

if ($values.clipboard) {
    Set-Clipboard $cypherPass
    Write-Host "$proccessItem password copied to clipboard"
    Write-Host ""
}

exit 0