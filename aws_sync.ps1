# --- protección contra falla de ejecución del script si es que se llama desde otro directorio - start ---
# --- ------------------------------------------------------------------------------------------------ ---
# ubicación del script
$script_folder='C:\aws_sync'

# obtengo current path
$current_path=$pwd.path
# path actual
$path=$current_path

# si el actual path no coincide con el path del script
if($current_path -ne $script_folder) {
    # voy a la carpeta del script
    cd $script_folder
}

# --- protección contra falla de ejecución del script si es que se llama desde otro directorio - end ---
# --- ---------------------------------------------------------------------------------------------- ---

# ----------------------------------------
# --- definición de argumentos validos ---
$params = @{
    src = @('--source','--src','-src');
    help = @('--help','-h','/?','-?');
    debug = @('--debug','-debug');
    notify_to = @('--send-to','--notify-to','-send-to','-notify-to');
    view_summary = @('--view-summary','-view-summary', '--summary-view','-summary-view');
    send_notify = @('--deliver-summary','-deliver-sumary','--deliver-notify','--send-notify','-send-notify','-deliver-notify');
    debug_params = @('--debug-params','-debug-params');
    debug_credentials = @('--debug-credentials','-debug-credentials');
    aws_debug = @('--debug-aws','-debug-aws','--daws','-daws','--aws-debug','-aws-debug','-awsd','--awsd');
    bucket = @('--bucketS3', '-s3', '-bucket', '--bucket', '--bucketname')
}

# --------------------------------------------------------------------
# --- hashtable que guarda indice, valor y si fue seteado el argumento
# --- según los argumentos que haya recibido el script
$args_params = @{
    src = @{ index = -1; is_required = $true; require_value = $true; setted = $false; value = "" };
    bucket = @{ index = -1; is_required = $true; require_value = $true; setted = $false; value = "" };
    notify_to = @{ index = -1; is_required = $false; require_value = $true; setted = $false; value = "" };
    send_notify = @{ index = -1; is_required = $false; require_value = $false; setted = $false; value = "" };
    view_summary = @{ index = -1; is_required = $false; require_value = $false; setted = $false; value = "" };
    aws_debug = @{ index = -1; require_value = $false; setted = $false; value = "" };
    debug = @{ index = -1; require_value = $false; setted = $false; value = "" };
    debug_credentials = @{ index = -1; require_value = $false; setted = $false; value = "" };
    help = @{ index = -1; require_value = $false; setted = $false; value = "" };
}

# -------------------------------------------------------
# --- definición de argumentos que no requieren valor ---
$params_without_values = $args_params.GetEnumerator() | ForEach-Object {
    if(-not $_.value.require_value) { $_.key }
}

# ----------------------------------------------------
# --- lugar donde alojo los valores de los argumentos
# --- recibidos en el script
$values = @{
    src = "";
    bucket = "";
    notify_to = "";
    send_notify = $false;
    view_summary = $false;
    debug = $false;
    debug_credentials = $false;
    aws_debug = $false
}
# ----------------------------------
# --- simbolos para denotar acciones
$proccessItem= "[#]"
$proccessResponse="[=]"
$proccessDebug="[d]"
$proccessDebugCredentials="[v]"
$proccessAwsSync = "[Sync AWS S3]"
$proccessDebugParams ="[?]"


Write-Host ""
Write-Host "----------------------"
Write-Host "#    Sync with AWS   #"
Write-Host "----------------------"
Write-Host "~"
Write-Host "$proccessDebugParams Argument debug"
Write-Host "$proccessItem Current Proccess"
Write-Host "$proccessDebug Proccess Debug"
Write-Host "$proccessDebugCredentials Proccess Debug Credentials"
Write-Host "$proccessResponse Proccess Response"
Write-Host "~"
Write-Host ""

$debug_params = $false
$debug_credentials = $false
if(($args.Count -gt 0)){
    foreach ($param in $params.debug_params) {
        $index = [array]::IndexOf($args, $param)
        if($index -gt -1) {
            $debug_params = $true
            break
        }
    }
    foreach ($param in $params.debug_credentials) {
        $index = [array]::IndexOf($args, $param)
        if($index -gt -1) {
            $debug_credentials = $true
            break
        }
    }
}

if($debug_params) {
    Write-Host "$proccessDebugParams [debug arguments]"
    Write-Host "$proccessDebugParams Total arguments: " $args.Count
}

# si recibió argumentos
if ($args.Count -gt 0){
    # navego cada param key
    foreach($param_key in $params.keys) {
        # Write-Host "[-] posible params: " $params[$param_key] # debug
        # por cada valor en los valores
        # Write-Host ">> param: $param_key" # debug
        foreach($param in $params[$param_key]) {
            $index = [array]::IndexOf($args, $param)
            if(($index -gt -1)) {
                # Write-Host "$proccessDebugParams $param in arguments" # debug
                # si no existe elemento es porque es un parametro especial
                if(-not $args_params[$param_key]) { continue }
                if(-not $args_params[$param_key].setted) {
                    $args_params[$param_key].index = $index
                    if($args_params[$param_key].require_value) {
                        $value_index = $args_params[$param_key].index + 1

                        $param_value = $args[$value_index]
                        $is_empty = [string]::IsNullOrEmpty($param_value)
                        if($debug_params) { Write-Host "$proccessDebugParams Param found: $param, value: $param_value" }

                        # Write-Host "$value_index, $is_empty" # debug
                        if($is_empty) {
                            throw "$param not has value"
                        } else {
                            if($param_value -like '-*'){
                                throw "$param not has value"
                            }
                        }
                        $args_params[$param_key].value = $param_value
                    } else {
                        if($param_key -in $params_without_values) {
                            if($param_key -like "*debug*") {
                                $readable_param = $param_key.replace('_',' ')
                                Write-Host "$proccessDebugParams $readable_param enabled"
                            } # debug
                            $args_params[$param_key].setted = $true
                            continue
                        }

                        if($debug_params) {
                            Write-Host "$proccessDebugParams $param"
                        } # debug
                    }
                    $args_params[$param_key].setted = $true
                }
            }
        }
    }
}
Write-Host ""

$verify_args = $true
if($args_params.help.setted) {
    $verify_args = $false
}

foreach ($param in $args_params.keys) {
    if(($param -ne "help") -and $verify_args) {
        if($args_params[$param].setted) {
            if($args_params[$param].require_value) {
                $values[$param] = $args_params[$param].value
            } else {
                $values[$param] = $args_params[$param].setted
            }
        } else {
            if($args_params[$param].is_required) {
                $_param = $params[$param]
                throw "$param not found and is required, use someone of this params to set it: $_param"
            }
        }
    }
}

$values.debug_credentials = $debug_credentials
if($values.notify_to -ne "" -and $values.send_notify -eq $false) {
    $_param = $params.send_notify
    throw "notify to is setted but send notify not, please use some of this params to set it: $_param"
}

if($values.send_notify -and $values.notify_to -eq "") {
    $_param = $params.send_notify
    throw "you set send notify but notify to not, please use some of this params to set it: $_param"
}

if ($values.aws_debug) {
    Write-Host "$proccessDebug AWS Debug enabled"
}

if ($values.debug) {
    Write-Host "$proccessDebug Debug enabled"
    Write-Host ""
    Write-Host "$proccessDebug [values]"
    $values | Format-Table
}

if($args_params.help.setted) {
    Write-Host "Params accepted:"
    foreach ($param in $params.keys) {
        $readable_param = $param.replace('_',' ')
        if($param -eq 'src') {
            $readable_param = 'source folder'
        }
        Write-Host "- For set $readable_param option: " ($params[$param] -join ", ")
    }
    Write-Host ""
    exit 0
}

if($debug_params) {
    Write-Host ""
    Write-Host "-------------------------------------"
    Write-Host ""
    Write-Host "[src]"
    $args_params.src | Format-Table

    Write-Host "[bucket]"
    $args_params.bucket | Format-Table
}
Write-Host "$proccessItem Verify if AWS is installed..."

# obtengo aws ejecutable
$aws = Get-Command aws -ErrorAction SilentlyContinue
# si no existe ejecutable
if (-not $aws) {
    Write-Host "$proccessResponse AWS is not installed"
    Write-Host ""
    Write-Host "$proccessItem Start setup AWS CLI"

    # Ejecuto instalador
    Start-Process msiexec.exe -ArgumentList "/i https://awscli.amazonaws.com/AWSCLIV2.msi /qb!" -Wait
    Write-Host "$proccessItem Updating current environment..."

    # Actualizo environment
    $env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine")
    Write-Host "$proccessItem Updated environment complete"
    # verifico que se haya instalado aws
    Start-Process aws -ArgumentList "$proccessItem-version" -ErrorAction SilentlyContinue
    if ($LASTEXITCODE -eq 0) {
        Write-Host "$proccessResponse Setup is completted"
        Write-Host ""
    }
} else {
    Write-Host "$proccessResponse AWS is installed"
    Write-Host ""
}

Write-Host "$proccessItem Updating system environment with aws variables from the file $aws_env"
# leo archivo .env para las credenciales
# estableciendo las variables en el sistema operativo
$aws_env = './.aws.env'

Write-Host ""
Write-Host "$proccessDebugCredentials [credentials]"
if($debug_credentials) {
    Write-Host "$proccessDebugCredentials reading $aws_env..."
}
foreach ($line in (& get-content $aws_env)) {
    if ($line -match "([^=]+)=(.*)") {
        $key = $matches[1]
        $value = $matches[2] -replace '^"|"$'
        Set-Item -Path "env:$key" -Value $value

        if($debug_credentials) { Write-Host "$proccessDebugCredentials $key=$value" }
    }
}

# leo variable env, si esta todo ok deberían figurar a nivel sistema operativo
$region = $Env:AWS_DEFAULT_REGION
if (-not $region) {
    Write-Host "$proccessResponse Something was wrong when update system environment with aws variables"
    exit $LASTEXITCODE
} else {
    Write-Host "$proccessResponse Updated environment with aws variables complete"
}

# obtengo bucket y folder para la sincronización
$aws_s3_bucket = $values.bucket
$folder_to_sync = $values.src

Write-Host ""
Write-Host "$proccessItem Syncing $folder_to_sync into s3://$aws_s3_bucket"

$debug_on_aws=""
if($values.aws_debug) {
    $debug_on_aws="--debug"
}
$args_aws = "s3 sync $folder_to_sync s3://$aws_s3_bucket --delete --exact-timestamps $debug_on_aws"
if($values.debug) {
    Write-Host "$proccessDebug aws $args_aws"
}

$output_file = ".\output.txt"
$error_file = ".\error.txt"
# ejecuto sincronización
Start-Process aws -ArgumentList $args_aws -NoNewWindow -Wait -RedirectStandardOutput $output_file -RedirectStandardError $error_file

if($values.debug) {
    if([string]::IsNullOrEmpty($LASTEXITCODE)) {
        Write-Host "$proccessDebug $proccessAwsSync Exit-Code Empty"
    } else {
        Write-Host "$proccessDebug $proccessAwsSync Exit-Code $LASTEXITCODE"
    }
}

if (($LASTEXITCODE -eq 0) -or ($LASTEXITCODE -eq 1) -or ([string]::IsNullOrEmpty($LASTEXITCODE))) {
    Write-Host "$proccessResponse Sync completed for folder $folder_to_sync"
    Write-Host ""
} else {
    if($LASTEXITCODE -eq 1) {
        Write-Host "$proccessResponse $proccessAwsSync Error: Limited to s3 commands, at least one or more s3 transfers failed for the command executed"
    }
    if($LASTEXITCODE -eq 2) {
        Write-Host "$proccessResponse $proccessAwsSync Error: The meaning of this return code depends on the command being run"
    }
}

# Genero reporte - start
$sumary_file = ".\summary.txt"
Set-Content $sumary_file "----------------"
Add-Content $sumary_file "AWS Sync Summary"
Add-Content $sumary_file "----------------"

Add-Content $sumary_file "[running at]"
$readable_date=Get-Date -Format "dd/MM/yyyy HH:mm:ss"
Add-Content $sumary_file -Value $readable_date
Add-Content $sumary_file ""

Add-Content $sumary_file "[exit code]"
Add-Content $sumary_file -Value $LASTEXITCODE
Add-Content $sumary_file ""

Add-Content $sumary_file "[aws output]"
$output=(Get-Content $output_file)
# quito texto: Completed NN MiB/~MMM MiB (AAA MiB/s) with ~BBB file(s) remaining (calculating...)
# 1ro reemplazo esas lineas por ''
$output=($output | ForEach-Object { $_ -replace 'Completed(?s).*remaining|(\ \(calculating\.\.\.\))','' })
# 2do borro lineas vacias
$output=($output | where { $_.trim() -ne "" })

Add-Content $sumary_file -Value ($output)
Add-Content $sumary_file ""

Add-Content $sumary_file "[aws error]"
Add-Content $sumary_file -Value (Get-Content $error_file)
Add-Content $sumary_file ""
Add-Content $sumary_file "----------------"
# Genero reporte - end

Write-Host "$proccessItem Starting post sync"
# Muestro reporte - start
$summary=(Get-Content $sumary_file)
$proccessSummary = $proccessResponse
if($values.view_summary -and -not $values.debug) {
    Write-Host "$proccessSummary [summary info]"
}
if(-not $values.view_summary -and $values.debug) {
    Write-Host "$proccessDebug [summary info]"
}
if($values.view_summary -or $values.debug) {
    $summary
}
# Muestro reporte - end


# Mailing section - start
if($values.send_notify) {
    Write-Host ""
    Write-Host "$proccessDebugCredentials [email credentials]"
    # leo archivo .env para las credenciales del correo
    # estableciendo las variables en el sistema operativo
    $email_env = './.email.env'
    if($debug_credentials) {
        Write-Host "$proccessDebugCredentials reading $email_env..."
    }
    foreach ($line in (& get-content $email_env)) {
        if ($line -match "([^=]+)=(.*)") {
            $key = $matches[1]
            $value = $matches[2] -replace '^"|"$'
            Set-Item -Path "env:$key" -Value $value

            if($debug_credentials) { Write-Host "$proccessDebugCredentials $key=$value" }
        }
    }

    $user = $Env:SENDER_EMAIL_USER
    if (-not $user) {
        Write-Host "$proccessResponse Something was wrong when update system environment with email variables"
        exit $LASTEXITCODE
    } else {
        Write-Host "$proccessResponse Updated environment with email variables complete"
    }
    Write-Host ""

    $UserName = $Env:SENDER_EMAIL_USER
    $pass = [System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String($Env:SENDER_EMAIL_PASS))
    $securePwd =  ConvertTo-SecureString $pass -AsPlainText -Force
    $Credential = New-Object System.Management.Automation.PSCredential -ArgumentList ($UserName, $securePwd)

    $From = $UserName
    $To = $values.notify_to
    $Subject = 'AWS Sync Summary'
    $Body = ''
    foreach($line in Get-Content $sumary_file) {
        $Body = $Body+$line+"`n"
    }

    Write-Host "$proccessItem [send email]"
    Write-Host "$proccessItem notify to:"
    $To
    Write-Host ""

    $EmailParams = @{
        Encoding = "UTF8"
        From = $From
        To = $To
        Subject = $Subject
        Body = $Body
        Credential = $Credential
        Port = 587
        SmtpServer = 'smtp.gmail.com'
        Priority = "High"
        DeliveryNotificationOption = "OnSuccess"
        Verbose=$false
        UseSsl=$true
    }

    Send-MailMessage  @EmailParams
    if($LASTEXITCODE -eq 0) {
        Write-Host "$proccessItem email sended successfully"
    }
    if($values.debug) {
        Write-Host ""
    }
}
# Mailing section - end

# clean section - start
Remove-Item $output_file
Remove-Item $error_file
Remove-Item $sumary_file
# clean section - end

# si el path desde donde se llamo el script no es el mismo que la carpeta del script
if($path -ne $current_path) {
    # vuelvo a la carpeta desde donde se llamo
    cd $current_path
}

exit 0