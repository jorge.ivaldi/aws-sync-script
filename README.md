# AWS Sync Script
<br>
<img src="img/AWS.png" width="196" height="196" alt="AWS S3 Cloud"><br>

- El script requiere acceso a internet

- El script instala automaticamente AWS CLI, si este no se encuentra ya instalado en la pc

- Debe definirse un archivo **.aws.env** junto al script que debe tener el siguiente contenido:

```dosini
# .aws.env
AWS_ACCESS_KEY_ID="..access_key_id.."
AWS_SECRET_ACCESS_KEY="..secret_access_key.."
AWS_DEFAULT_REGION="..region-bucket.."
```
- El script automaticamente irá a la carpeta donde figura el script si se llama desde otro lugar y volverá al terminar a esa carpeta

# Como correr el script
Es necesario correr script como administrador

Para ver apartado de ayuda
```ps1
aws_sync.ps1 --help
```
<img src="img/examples/aws_sync-help.png" alt="Obtener ayuda"><br>

Ejecutar comando
```ps1
aws_sync.ps1 -src $absolute_path_src -bucket $bucket_name
```
<img src="img/examples/aws_sync-only.png" alt="Generar sincronización"><br>

Ejecutar comando con emisión de reporte
```ps1
aws_sync.ps1 -src $absolute_path_src -bucket $bucket_name --summary-view
```
<img src="img/examples/aws_sync-with-summary-info-1.png" alt="Generar sincronización"><br>
<img src="img/examples/aws_sync-with-summary-info-2.png" alt="Generar sincronización"><br>


Ejecutar comando con envio de correo
```ps1
aws_sync.ps1 -src $absolute_path_src -bucket $bucket_name --deliver-notify --notify-to {email_to_send}
```
<img src="img/examples/aws_sync-with-deliver-notify-1.png" alt="Generar sincronización y notificar por correo (ejemplo 1)"><br>

Sincronización correcta
<img src="img/examples/aws_sync-with-deliver-notify-2.png" alt="Notificacion de reporte de sincronización (ejemplo 1)"><br>

Error en sincronización
<img src="img/examples/aws_sync-with-deliver-notify-3.png" alt="Notificacion de reporte de sincronización (ejemplo 2)"><br>



Ejecutar comando con debug params
```ps1
aws_sync.ps1 -src $absolute_path_src -bucket $bucket_name --debug-params
```

Ejecutar comando con debug credentials
```ps1
aws_sync.ps1 -src $absolute_path_src -bucket $bucket_name --debug-credentials
```

Ejecutar comando con debug
```ps1
aws_sync.ps1 -src $absolute_path_src -bucket $bucket_name --debug
```

Ejecutar comando con aws debug
```ps1
aws_sync.ps1 -src absolute_path -bucket bucket_name --aws-debug
```

# Datos de utilidad
## Configuración necesaria en aws
#### (1) Se debe crear cuenta de usuario con todos los privilegios
<img src="img/create-user.png" alt="Crear cuenta de usuario"><br>

#### (2) Crear archivo **.aws.env** donde se encuentre el script con las credenciales de acceso:
<img src="img/access-key.png" alt="Credenciales de acceso"><br>

#### (3) Crear un bucket nuevo
<img src="img/create-bucket.png" alt="Crear bucket nuevo"><br>


## Como agregar script como tarea programada para que sincronize y notifique sincronización por correo
#### (1) Generar archivo .email.env
- Primero debes obtener la clave de tu correo cifrada, para ello usa el script tool de ```cypher-pass.ps1``` con la opción **--clipboard** para que lo copie al portapapeles

```ps1
.\cypher-pass.ps1 --pwd ClaveDeMiCorreo --clipboard
```
<img src="img/examples/cypher-pass.png" alt="Obtener clave cifrada"><br>

- Luego crea un archivo llamado .email.env donde esta el script ```aws_sync.ps1``` con el siguiente contenido, pegando la clave que obtuviste del paso anterior:
```.env
# .email.env
SENDER_EMAIL_USER="mi-correo@email-provider.com"
SENDER_EMAIL_PASS="Q2xhdmVEZU1pQ29ycmVv"
```

#### (2) Crear una tarea:
- En la solapa Acciones, crear una nueva con las siguientes especificaciones:
    * Acción: Iniciar un Programa
    * Programa o Script: ```powershell.exe```
    * Agregar argumentos: ```-NoProfile -NonInteractive -ExecutionPolicy Unrestricted -Command "C:\aws_sync\aws_sync.ps1 -bucket {bucket_name} -src {absolute_path_src} --deliver-notify --notify-to {email_to_send_1}, {email_to_send_2}"```
- En la solapa Desencadenadores:
    - Especificar la periodisidad
- En la solapa General:
    - Ejecutar con los privilegios más altos

## Como agregar script como tarea programada para que solo sincronize
#### Crear una tarea:
- En la solapa Acciones, crear una nueva con las siguientes especificaciones:
    * Acción: Iniciar un Programa
    * Programa o Script: ```powershell.exe```
    * Agregar argumentos: ```-NoProfile -NonInteractive -ExecutionPolicy Unrestricted -Command "C:\aws_sync\aws_sync.ps1 -bucket {bucket_name} -src {absolute_path_src}"```
- En la solapa Desencadenadores:
    - Especificar la periodisidad
- En la solapa General:
    - Ejecutar con los privilegios más altos
